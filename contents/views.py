# coding=utf8
from django.shortcuts import render, get_object_or_404
from django.template import loader, RequestContext
from django.shortcuts import render_to_response
from datetime import date, datetime, timedelta
from collections import deque
from django.http import Http404
from django.http import HttpResponse
from django.core.mail import send_mail

import time
from email.Utils import formatdate 

def datetime2rfc(dt): 
    dt = dt - timedelta(hours=2)
    dt = time.mktime(dt.timetuple())
    return formatdate(dt, usegmt=True)

from contents import models

def contact_proc(request):
    contact = get_object_or_404(models.Contact, pk=1)
    now_date = date.today()
    return {
        'contact': contact,
        'year': now_date.year
    }

def index(request):
    latest_project_list = models.Project.objects.order_by('-date')[:4]
    company_info = models.CompanyInfo.objects.filter(site_part='orig')
    main_teasers = models.MainTeaser.objects.all()
    html_attributes = get_object_or_404(models.HtmlAttributes, page_type='mn')
    try:
        services_to_main = models.Service.objects.filter(main_page=True)[0:4]
    except IndexError:
        services_to_main = None
    c = RequestContext(request, processors=[contact_proc])
    response =  render_to_response(
        'index.html', 
        dict(latest_project_list=latest_project_list,
             services_to_main=services_to_main, 
             company_info=company_info, 
             main_teasers=main_teasers, 
             html_attributes=html_attributes),
        c)
    response['Last-Modified'] = datetime2rfc(datetime.now())
    return response

def contact(request):
    teaser = models.SubTeaser.objects.latest('id');#get_object_or_404(models.SubTeaser, pk=1)
    contacts = get_object_or_404(models.Contact, pk=1)
    html_attributes = get_object_or_404(models.HtmlAttributes, page_type='ct')
    c = RequestContext(request, processors=[contact_proc])
    response =  render_to_response(
        'contact.html', 
        dict(contact=contacts, contacts=contacts, teaser=teaser, html_attributes=html_attributes),c)
    response['Last-Modified'] = datetime2rfc(datetime.now())
    return response

def services(request):
    services = models.Service.objects.all()
    teaser = models.SubTeaser.objects.latest('id');#get_object_or_404(models.SubTeaser, pk=1)
    html_attributes = get_object_or_404(models.HtmlAttributes, page_type='sr')
    c = RequestContext(request, processors=[contact_proc])
    response = render_to_response(
        'services.html', 
        dict(services=services, teaser=teaser, html_attributes=html_attributes), c)
    response['Last-Modified'] = datetime2rfc(datetime.now())
    return response

def service_details(request, service_alias):
    if models.Service.objects.filter(page_alias=service_alias).count():
        service = get_object_or_404(models.Service, page_alias=str(service_alias))
    elif service_alias.isdigit():
        service = get_object_or_404(models.Service, pk=int(service_alias))
    else:
        raise Http404()
    c = RequestContext(request, processors=[contact_proc])
    if service.special_service:
        html_attributes = get_object_or_404(models.HtmlAttributes, page_type='mn')
        special_project_list = models.Project.objects.filter(site_part='spec')[:4]
        special_company_info = models.CompanyInfo.objects.filter(site_part='spec')
        main_teasers = models.MainTeaser.objects.all()
        try:
            special_services = models.Service.objects.filter(site_part='spec')[:4]
        except IndexError:
            special_services = None
        c = RequestContext(request, processors=[contact_proc])
        response = render_to_response(
            'index.html',
            dict(latest_project_list=special_project_list,
                 services_to_main=special_services,
                 company_info=special_company_info,
                 main_teasers=main_teasers,
                 html_attributes=html_attributes),
            c)
    else:
        teaser = models.SubTeaser.objects.latest('id');#get_object_or_404(models.SubTeaser, pk=1)
        html_attributes = get_object_or_404(models.HtmlAttributes, page_type='sr')
        response = render_to_response(
            'service_details.html',
            dict(service=service, teaser=teaser, html_attributes=html_attributes), c)
    response['Last-Modified'] = datetime2rfc(datetime.now())
    return response

def projects(request):
    projects = models.Project.objects.all()
    teaser = models.SubTeaser.objects.latest('id');#get_object_or_404(models.SubTeaser, pk=1)
    c = RequestContext(request, processors=[contact_proc])
    html_attributes = get_object_or_404(models.HtmlAttributes, page_type='pr')
    response =  render_to_response(
        'projects.html',
        dict(projects=projects, teaser=teaser, html_attributes=html_attributes),c)
    response['Last-Modified'] = datetime2rfc(datetime.now())
    return response

def project_details(request, project_id):
    project = get_object_or_404(models.Project, pk=project_id)
    teaser = models.SubTeaser.objects.latest('id');#get_object_or_404(models.SubTeaser, pk=1))
    c = RequestContext(request, processors=[contact_proc])
    html_attributes = get_object_or_404(models.HtmlAttributes, page_type='pr')
    response =  render_to_response(
        'project_details.html',
        dict(project=project, teaser=teaser, html_attributes=html_attributes),c)
    response['Last-Modified'] = datetime2rfc(datetime.now())
    return response

def sitemap(request):
    teaser = models.SubTeaser.objects.latest('id');
    
    c = RequestContext(request, processors=[contact_proc])
    
    project_attributes = get_object_or_404(models.HtmlAttributes, page_type='pr')
    service_attributes = get_object_or_404(models.HtmlAttributes, page_type='sr')
    contact_attributes = get_object_or_404(models.HtmlAttributes, page_type='ct')
    
    response =  render_to_response(
        'sitemap.html',
        dict(teaser=teaser, 
             project_attributes=project_attributes, 
             service_attributes=service_attributes,
             contact_attributes=contact_attributes),c)
    response['Last-Modified'] = datetime2rfc(datetime.now())
    return response

def callback(request):
    message = u'Имя пользователя: '+request.POST.get('name')+"\n"
    message += u'Телефон: '+request.POST.get('tel')+"\n"
    send_mail(u'Заказ колбека!', message, 'noreply@loochpictures.ru', ['hello@loochpictures.ru'])
    return HttpResponse(u'Ваша заявка принята, скоро мы перезвоним Вам.')

def order(request):
    message = u'Имя пользователя: '+request.POST.get('user_name')+"\n"
    message += u'email: '+request.POST.get('email')+"\n"
    message += u'Телефон: '+request.POST.get('tel')+"\n"
    message += u'Комментарий: '+request.POST.get('message', '')+"\n"
    send_mail(u'Заявка!', message, 'noreply@loochpictures.ru', ['hello@loochpictures.ru'])
    return HttpResponse(u'Ваша заявка принята, скоро мы свяжемся с Вами.')
    
def tpl404(request):
    teaser = models.SubTeaser.objects.latest('id')
    c = RequestContext(request, processors=[contact_proc])
    response =  render_to_response(
        '404.html',
        dict(teaser=teaser), c)
    return response
