# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0004_auto_20141203_2229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companyinfo',
            name='site_part',
            field=models.CharField(default=b'orig', max_length=4, verbose_name='\u0422\u0438\u043f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u0438', choices=[(b'orig', '\u041e\u0431\u044b\u043a\u043d\u043e\u0432\u0435\u043d\u043d\u044b\u0439'), (b'spec', '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0439')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='project',
            name='site_part',
            field=models.CharField(default=b'orig', max_length=4, verbose_name='\u0422\u0438\u043f \u043f\u0440\u043e\u0435\u043a\u0442\u0430', choices=[(b'orig', '\u041e\u0431\u044b\u043a\u043d\u043e\u0432\u0435\u043d\u043d\u044b\u0439'), (b'spec', '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0439')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='service',
            name='site_part',
            field=models.CharField(default=b'orig', max_length=4, verbose_name='\u0422\u0438\u043f \u0443\u0441\u043b\u0443\u0433\u0438', choices=[(b'orig', '\u041e\u0431\u044b\u043a\u043d\u043e\u0432\u0435\u043d\u043d\u044b\u0439'), (b'spec', '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u044b\u0439')]),
            preserve_default=True,
        ),
    ]
