# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='page_alias',
            field=models.CharField(default=b'', max_length=255, verbose_name='\u0410\u043b\u0438\u0430\u0441', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='service',
            name='page_h1',
            field=models.CharField(default=b'', max_length=600, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a H1', blank=True),
            preserve_default=True,
        ),
    ]
