# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0432\u044b\u0432\u043e\u0434\u0430', db_index=True)),
            ],
            options={
                'ordering': ['ordering'],
                'verbose_name': '\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438',
                'verbose_name_plural': '\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('adress', models.CharField(max_length=100, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('phone', models.CharField(max_length=100, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('fax', models.CharField(max_length=100, verbose_name='\u0424\u0430\u043a\u0441')),
                ('email', models.CharField(max_length=100, verbose_name='Email')),
                ('gl_country', models.CharField(max_length=50, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430')),
                ('gl_city', models.CharField(max_length=50, verbose_name='\u0413\u043e\u0440\u043e\u0434')),
                ('gl_street', models.CharField(max_length=50, verbose_name='\u0423\u043b\u0438\u0446\u0430')),
                ('gl_bld', models.CharField(max_length=5, verbose_name='\u0414\u043e\u043c')),
                ('gl_korpus', models.CharField(max_length=5, verbose_name='\u041a\u043e\u0440\u043f\u0443\u0441', blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b',
                'verbose_name_plural': '\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HtmlAttributes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page_type', models.CharField(default=b'mn', max_length=2, choices=[(b'mn', '\u0413\u043b\u0430\u0432\u043d\u0430\u044f'), (b'sr', '\u0421\u0435\u0440\u0432\u0438\u0441\u044b'), (b'pr', '\u041f\u0440\u043e\u0435\u043a\u0442\u044b'), (b'ct', '\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b')])),
                ('page_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title" \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b')),
                ('page_description', models.TextField(verbose_name='\u041c\u0435\u0442\u0430 \u0442\u0435\u0433 "description"')),
                ('page_keywords', models.TextField(verbose_name='\u041c\u0435\u0442\u0430 \u0442\u0435\u0433 "keywords"')),
            ],
            options={
                'verbose_name': 'HTML \u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0434\u043b\u044f \u043e\u0441\u043d\u043e\u0432\u043d\u044b\u0445 \u0441\u0442\u0440\u0430\u043d\u0438\u0446',
                'verbose_name_plural': 'HTML \u0430\u0442\u0440\u0438\u0431\u0443\u0442\u044b \u0434\u043b\u044f \u043e\u0441\u043d\u043e\u0432\u043d\u044b\u0445 \u0441\u0442\u0440\u0430\u043d\u0438\u0446',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 940*529 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'projects/images', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f')),
                ('html_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title"')),
                ('html_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0432\u044b\u0432\u043e\u0434\u0430', db_index=True)),
            ],
            options={
                'ordering': ('ordering',),
                'verbose_name': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MainTeaser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 1920x550 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'mainteaser/images', verbose_name='\u0424\u043e\u0442\u043e')),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0432\u044b\u0432\u043e\u0434\u0430', db_index=True)),
                ('html_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title"')),
                ('html_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
            ],
            options={
                'ordering': ['ordering'],
                'verbose_name': '\u0422\u0438\u0437\u0435\u0440 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435',
                'verbose_name_plural': '\u0422\u0438\u0437\u0435\u0440\u044b \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0440\u043e\u0435\u043a\u0442\u0430')),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('short_description', models.TextField(verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('cover_image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 281x281 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'projects/cover', verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430')),
                ('html_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title"')),
                ('html_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
                ('page_description', models.TextField(verbose_name='\u041c\u0435\u0442\u0430 \u0442\u0435\u0433 "description"')),
                ('page_keywords', models.TextField(verbose_name='\u041c\u0435\u0442\u0430 \u0442\u0435\u0433 "keywords"')),
            ],
            options={
                'ordering': ['-date'],
                'verbose_name': '\u041f\u0440\u043e\u0435\u043a\u0442',
                'verbose_name_plural': '\u041f\u0440\u043e\u0435\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.URLField(verbose_name='\u0410\u0434\u0440\u0435\u0441 Vimeo')),
                ('video_image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 940*529 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'projects/video/images', verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430 \u0434\u043b\u044f \u0432\u0438\u0434\u0435\u043e')),
                ('vimeo_number', models.CharField(max_length=50, verbose_name='\u041d\u043e\u043c\u0435\u0440 Vimeo')),
                ('image_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
                ('image_title', models.CharField(max_length=100, verbose_name='"Title"')),
                ('project', models.ForeignKey(to='contents.Project')),
            ],
            options={
                'verbose_name': '\u0412\u0438\u0434\u0435\u043e',
                'verbose_name_plural': '\u0412\u0441\u0435 \u0432\u0438\u0434\u0435\u043e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('short_description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0432\u044b\u0432\u043e\u0434\u0430', db_index=True)),
                ('image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 940*529 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'services/images', verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430 \u0434\u043b\u044f \u0443\u0441\u043b\u0443\u0433\u0438')),
                ('image_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
                ('image_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title"')),
                ('main_page', models.BooleanField(default=False, db_index=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435')),
                ('veryshort_description', models.TextField(verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('page_description', models.TextField(verbose_name='\u041c\u0435\u0442\u0430 \u0442\u0435\u0433 "description"')),
                ('page_keywords', models.TextField(verbose_name='\u041c\u0435\u0442\u0430 \u0442\u0435\u0433 "keywords"')),
            ],
            options={
                'ordering': ['ordering'],
                'verbose_name': '\u0423\u0441\u043b\u0443\u0433\u0430',
                'verbose_name_plural': '\u0423\u0441\u043b\u0443\u0433\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ServiceImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 940*529 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'services/images', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f')),
                ('html_title', models.CharField(max_length=100, verbose_name='"Title"')),
                ('html_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0432\u044b\u0432\u043e\u0434\u0430', db_index=True)),
                ('service', models.ForeignKey(to='contents.Service')),
            ],
            options={
                'ordering': ('ordering',),
                'verbose_name': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ServiceVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.URLField(verbose_name='\u0410\u0434\u0440\u0435\u0441 Vimeo')),
                ('video_image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 940*529 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'services/video/images', verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430 \u0434\u043b\u044f \u0432\u0438\u0434\u0435\u043e')),
                ('vimeo_number', models.CharField(max_length=50, verbose_name='\u041d\u043e\u043c\u0435\u0440 Vimeo')),
                ('image_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
                ('image_title', models.CharField(max_length=100, verbose_name='"Title"')),
                ('service', models.ForeignKey(to='contents.Service')),
            ],
            options={
                'verbose_name': '\u0412\u0438\u0434\u0435\u043e',
                'verbose_name_plural': '\u0412\u0441\u0435 \u0432\u0438\u0434\u0435\u043e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 281x281 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'subservices/images', verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f')),
                ('is_cover_image', models.BooleanField(default=False, db_index=True, verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430?')),
                ('html_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title"')),
                ('html_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
            ],
            options={
                'verbose_name': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('descritpion', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 281x281 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'subservices/images', verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430')),
                ('image_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
                ('image_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title"')),
                ('service', models.ForeignKey(to='contents.Service')),
            ],
            options={
                'verbose_name': '\u041f\u043e\u0434\u043f\u0443\u043d\u043a\u0442 \u0443\u0441\u043b\u0443\u0433\u0438',
                'verbose_name_plural': '\u041f\u043e\u0434\u043f\u0443\u043d\u043a\u0442\u044b \u0443\u0441\u043b\u0443\u0433\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubTeaser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(help_text='\u0420\u0430\u0437\u043c\u0435\u0440 1920x550 \u043f\u0438\u043a\u0441\u0435\u043b\u0435\u0439.', upload_to=b'subteaser/images', verbose_name='\u0424\u043e\u0442\u043e')),
                ('html_title', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Title"')),
                ('html_alt', models.CharField(max_length=100, verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442 "Alt"')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u0437\u0435\u0440 \u043d\u0430 \u0434\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0445 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430\u0445',
                'verbose_name_plural': '\u0422\u0438\u0437\u0435\u0440 \u043d\u0430 \u0434\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0445 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430\u0445',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='subimage',
            name='service',
            field=models.ForeignKey(to='contents.SubService'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='image',
            name='project',
            field=models.ForeignKey(to='contents.Project'),
            preserve_default=True,
        ),
    ]
