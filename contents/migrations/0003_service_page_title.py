# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0002_auto_20141101_1714'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='page_title',
            field=models.CharField(default=b'', max_length=255, verbose_name='\u0422\u0430\u0439\u0442\u043b', blank=True),
            preserve_default=True,
        ),
    ]
