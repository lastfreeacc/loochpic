# -*- coding: utf-8 -*-
from django.http import HttpResponsePermanentRedirect
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic.base import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from contents import views
from contents.sitemap import *


sitemaps = {
    'index': MainSitemap,
    'project': ProjectSitemap,
    'service': ServiceSitemap,
    'other': OtherSitemap,
}

handler404 = views.tpl404

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myproject.views.home', name='home'),
    url(r'^$', views.index, name='index'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^services/$', views.services, name='services'),
    (r'^service_details/4/$', lambda request: HttpResponsePermanentRedirect('/service_details/3d-mapping-shou/')),
    (r'^service_details/7/$', lambda request: HttpResponsePermanentRedirect('/service_details/interaktivnye-sistemy/')),
    (r'^service_details/6/$', lambda request: HttpResponsePermanentRedirect('/service_details/izgotovlenie-videorolikov/')),
    url(r'^service_details/(?P<service_alias>\S+)/$', views.service_details, name='service_details'),
    url(r'^projects/$', views.projects, name='projects'),
    url(r'^sitemap/$', views.sitemap),
    url(r'^order/$', views.order),
    url(r'^callback/$', views.callback),
    url(r'^project_details/(?P<project_id>\d+)/$', views.project_details, name='project_details'),
    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^yandex_72fb362f4e1186ce\.txt$', TemplateView.as_view(template_name='yandex_72fb362f4e1186ce.txt', content_type='text/plain')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
                       
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))

#if settings.DEBUG:
    #urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)